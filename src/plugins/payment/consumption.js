const deploySmartContract = require('../../utils/deploySmartContract');
const plugins = require('../../plugins');
const consumptionAbiPath = '../../artifacts/contracts/ConsumptionChecker.sol/ConsumptionChecker.json';
const consumptionAbi = require('../' + consumptionAbiPath).abi

async function generateSmartContract(data) {
    try {
        const contract = await deploySmartContract(data.consumption, consumptionAbiPath);
        await plugins.pluginsExecute('registry', 'saveSmartContractData', {
            address: contract,
            abi: JSON.stringify(consumptionAbi),
            label: "consumption",
            contractDId: data.contractDId
        }, {})
        return contract;
    } catch (err) {
        console.log(err);
        throw new Error('Consumption SC generation failed')
    }
}

module.exports = { generateSmartContract };




