const deploySmartContract = require('../../utils/deploySmartContract');
const plugins = require('../../plugins');
const billingAbiPath = '../../artifacts/contracts/BillingChecker.sol/BillingChecker.json';
const billingAbi = require('../' + billingAbiPath).abi
const axios= require('axios');

async function generateSmartContract(data) {
    try {
        data.billing.currency = "EUR"
        data.contractDId = "did:web:aster-x.demo23.gxfs.fr:d40fccec-ff23-4675-92df-eb04c9cb86ae:data.json"
        console.log("generateSmartContractBilling")
        const contract = await deploySmartContract(data.billing, billingAbiPath);
        await plugins.pluginsExecute('registry', 'saveSmartContractData', {
            address: contract,
            abi: JSON.stringify(billingAbi),
            label: "billing",
            contractDId: "did:web:aster-x.demo23.gxfs.fr:d40fccec-ff23-4675-92df-eb04c9cb86ae:data.json"
        }, {})
        await axios.post(`${process.env.ORACLE_URL}/bill/cron`, {... {
                periodicity: "PT10S",
                pricing: data.pricing,
                currency: data.currency,
            },
            address: contract,
            contractDId: data.contractDId
        });
        return contract;
    } catch (err) {
        console.log(err);
        throw new Error('Billing SC generation failed')
    }
}

module.exports = { generateSmartContract };




