// SPDX-License-Identifier: None
pragma solidity ^0.8.0;

contract PaymentChecker {
    struct Payment {
        uint paymentId;
        string externalInvoiceId;
        uint amount;
        uint paymentDate;
        string invoiceIndices;
        string paymentSolution;
        string status;
    }

    Payment[] public payments;

    event PaymentSaved(
        uint indexed paymentId,
        uint amount,
        uint paymentDate,
        string invoiceIndices,
        string paymentSolution,
        string status
    );

    function savePayment(
        string[] calldata _ids,
        uint[] calldata _amounts,
        string calldata paymentSolution
    ) external {
        require(
            _ids.length == _amounts.length,
            "Array lengths must match"
        );

        uint totalAmount;
        string memory invoiceIndices;

        for (uint256 i = 0; i < _ids.length; i++) {
            totalAmount += _amounts[i];
            if (i == 0) {
                invoiceIndices = _ids[i];
            } else {
                invoiceIndices = string(abi.encodePacked(invoiceIndices, ";", _ids[i]));
            }
        }

        uint paymentId = payments.length;

        payments.push(
            Payment({
                paymentId: paymentId,
                externalInvoiceId: '',
                amount: totalAmount,
                paymentDate: block.timestamp,
                invoiceIndices: invoiceIndices,
                paymentSolution: paymentSolution,
                status: 'C'
            })
        );

        emit PaymentSaved(paymentId, totalAmount, block.timestamp, invoiceIndices, paymentSolution, 'C');
    }

    function confirmPayment(uint paymentId) external {
        for (uint i = 0; i < payments.length; i++) {
            if (payments[i].paymentId == paymentId) {
                payments[i].status = 'P';
                return;
            }
        }
        revert("Payment with given paymentId not found");
    }

    function updateExternalId(uint paymentId, string calldata externalInvoiceId) external {
        for (uint i = 0; i < payments.length; i++) {
            if (payments[i].paymentId == paymentId) {
                payments[i].externalInvoiceId = externalInvoiceId;
                return;
            }
        }
        revert("Payment with given paymentId not found");
    }

    function getPayment(uint256 _paymentId)
    external
    view
    returns (
        uint256 paymentId,
        string memory externalInvoiceId,
        uint256 amount,
        uint256 paymentDate,
        string memory invoiceIndices,
        string memory paymentSolution,
        string memory status
    )
    {
        require(_paymentId < payments.length, "Payment ID does not exist");
        Payment memory payment = payments[_paymentId];
        return (
            payment.paymentId,
            payment.externalInvoiceId,
            payment.amount,
            payment.paymentDate,
            payment.invoiceIndices,
            payment.paymentSolution,
            payment.status
        );
    }

    function getPayments() external view returns (Payment[] memory) {
        return payments;
    }
}
