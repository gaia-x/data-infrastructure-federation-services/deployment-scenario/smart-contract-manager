const { ethers, JsonRpcProvider } = require("ethers");

module.exports = async function deploySmartContract(contractArg, contractCompiledFilePath) {
    try {
        const contractABI = require(contractCompiledFilePath).abi
        const contractBytecode = require(contractCompiledFilePath).bytecode;
        const provider = new JsonRpcProvider(process.env.EVM_NODE_URL);
        const wallet = new ethers.Wallet(process.env.PRIVATE_KEY, provider);
        const ContractFactory = new ethers.ContractFactory(contractABI, contractBytecode, wallet);
        const output = [];
        for (const key in contractArg) {
          output.push(contractArg[key]);
        }
        const contractInstance = await  ContractFactory.deploy(...(output ? output : []))//.waitForDeployment();

        return await contractInstance.getAddress();

    } catch (err) {
        console.log("Error in deploying contract:");
        console.log(err);
    }
}




