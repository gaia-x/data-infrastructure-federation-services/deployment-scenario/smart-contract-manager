const cors = require('cors');
const express = require('express');
const bodyParser = require('body-parser');
const logger = require('./services/logger');
const HttpError = require('./utils/httpError');
const plugins = require('./plugins.js');
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('../openapi.json');

/* For local development */
if (process.env.NODE_ENV !== "production") require("dotenv").config();

/* Express Configuration */
const app = express();
const PORT = 4000;
app.use(bodyParser.json());
app.use(cors());

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.use('/', require('./routes/smartContractManagerRoutes'));

/* Error */
app.use((error, req, res, next) => {
    logger.error(error.message, {stackTrace: error.stack ? error.stack : 'No stack trace'});
    if (error instanceof HttpError && error.statusCode !== 500) {
        res.status(error.statusCode).json({error: error.message});
    } else {
        res.status(500).json({error: 'Internal server error'});
    }
});



app.listen(PORT, () => {
    logger.info(`server started at port ${PORT}`);
    plugins.pluginsLoad(app);

    // Initialize Registry
    plugins.pluginsExecute('registry', 'generateSmartContract', {}, {})
        .then((result) => {
            console.log(`Registry deployed successfully with address ${result.address}`);
        })
        .catch((err) => {
            console.error('Registry deployment failed:', err);
            throw new Error('Registry deployment failed: ' + err)
        });
});