const fs = require('fs');

class ConfigManager {
    constructor(configFilePath = './config.json') {
        this.configFilePath = configFilePath;
        this.config = {};
        this.loadConfig();
    }

    loadConfig() {
        try {
            const data = fs.readFileSync(this.configFilePath);
            this.config = JSON.parse(data);
        } catch (err) {
            console.error(`Error loading config file: ${err}`);
        }
    }

    saveConfig() {
        try {
            fs.writeFileSync(this.configFilePath, JSON.stringify(this.config, null, 4));
            console.log('Config file saved successfully');
        } catch (err) {
            console.error(`Error saving config file: ${err}`);
        }
    }

    set(keyOrObject, value) {
        if (typeof keyOrObject === 'object') {
            Object.assign(this.config, keyOrObject);
        } else {
            this.config[keyOrObject] = value;
        }
        this.saveConfig();
    }

    get(key) {
        return this.config[key];
    }
}

module.exports = ConfigManager;
