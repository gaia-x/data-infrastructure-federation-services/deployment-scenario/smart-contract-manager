const deploySmartContract = require('../../utils/deploySmartContract');
const configManager = new (require('../../utils/configManager'))();
const { ethers, JsonRpcProvider } = require("ethers");

const registryAbiPath = '../../artifacts/contracts/SmartContractRegistry.sol/SmartContractRegistry.json';
const registryAbi = require('../' + registryAbiPath).abi

async function generateSmartContract(data) {
    try {
        if (!(Object.keys(configManager.config).length === 0)) {
            const provider = new JsonRpcProvider(configManager.config.evm);
            if (!(await provider.getCode(configManager.config.address) === "0x"))
                return configManager.config
        }
        const contract = await deploySmartContract("", registryAbiPath)
        configManager.set({ ...data, address: contract })
        return contract;
    } catch (err) {
        console.log(err);
        throw new Error('Registry SC generation failed')
    }
}

async function retrieveRegistryData(data) {
    try {
        return ({
            address: configManager.get('address'),
            evm: configManager.get('evm'),
            key: configManager.get('key'),
        })
    } catch (err) {
        console.log(err);
        throw new Error('Retrieving registry SC info failed')
    }
}

async function retrieveSmartContractData(options) {
    try {
        const provider = new JsonRpcProvider(process.env.EVM_NODE_URL);
        const signer = new ethers.Wallet(process.env.PRIVATE_KEY, provider);
        const contract = new ethers.Contract(configManager.get('address'), registryAbi, signer);
        if (options.address) {
            return await contract.getSmartContractByAddress(options.address);
        } else if (options.label && options.contractDId) {
           return await contract.getSmartContractByLabelContractDId(options.label, options.contractDId);
        } else {
            console.log('Insufficient parameters: either address or (label and contractId) must be provided.');
        }
    } catch (err) {
        console.error('Error retrieving smart contract data:', err);
    }
}

async function saveSmartContractData(info) {
    try {
        const provider = new JsonRpcProvider(process.env.EVM_NODE_URL);
        const signer = new ethers.Wallet(process.env.PRIVATE_KEY, provider);
        const contract = new ethers.Contract(configManager.get('address'), registryAbi, signer);
        const smartContract = {
            address_: info.address,
            cron: '',
            abi_: info.abi,
            label: info.label,
            contractDId: info.contractDId
        };
        const tx = await contract.setSmartContract(smartContract);
        //await tx.wait();
        console.log("Smart contract data saved into Registry successfully");
    } catch (err) {
        console.error('Error saving smart contract data:', err);
        throw new Error('Saving SC info failed');
    }
}


module.exports = { generateSmartContract, retrieveRegistryData, retrieveSmartContractData, saveSmartContractData };