pragma solidity ^0.8.0;

contract BillingChecker {
    struct Pricing {
        string type_;
        uint volume;
        string volumeType;
        uint pricePerVolume;
    }

    struct Consumption { 
        string type_;
        uint volume;
    }

    struct Bill {
        uint id;
        uint creationDate; 
        uint startDate;
        uint endDate;
        uint totalAmount;
        string currency;
        uint[] billDetailIds;
    }

    struct BillDetail {
        string type_;
        uint volume;
        string volumeType;
        uint volumeUnit;
        uint pricePerVolume;
        uint totalAmount;
    }

    Bill[] public bills;
    BillDetail[] public billDetails;
    string public currency;
    mapping(string => Pricing) public pricingMapping;


    constructor(Pricing[] memory pricingArray, string memory _currency) {
        currency = _currency;
        for (uint i = 0; i < pricingArray.length; i++) {
            pricingMapping[pricingArray[i].type_] = pricingArray[i];
        }
    }

    function setPricing(Pricing[] calldata newPricing) public {
        for (uint i = 0; i < newPricing.length; i++) {
            pricingMapping[newPricing[i].type_] = newPricing[i];
        }
    }

    function setPricing(Pricing calldata newPricing) public {
        pricingMapping[newPricing.type_] = newPricing;
    }

    function setCurrency(string calldata _currency) public {
        currency = _currency;
    }

    function getPricingByType(string calldata _type) external view returns (Pricing memory) {
        return pricingMapping[_type];
    }

    function getBillsCount() external view returns (uint) {
        return bills.length;
    }

    function saveBill(Consumption[] calldata consumption, uint startDate, uint endDate) external {
        Bill memory newBill;
        newBill.id = bills.length;
        newBill.creationDate = block.timestamp;
        newBill.totalAmount = 0;
        newBill.currency = currency;
        newBill.startDate = startDate;
        newBill.endDate = endDate;
        newBill.billDetailIds = new uint[](consumption.length);
        
        for (uint256 i = 0; i < consumption.length; i++) {
            BillDetail memory newDetail; 
            newDetail.type_ = consumption[i].type_;
            newDetail.volume = consumption[i].volume;
            newDetail.volumeUnit = pricingMapping[consumption[i].type_].volume;
            newDetail.pricePerVolume = pricingMapping[consumption[i].type_].pricePerVolume;
            newDetail.volumeType = pricingMapping[consumption[i].type_].volumeType;
            newDetail.totalAmount = (consumption[i].volume / pricingMapping[consumption[i].type_].volume) * pricingMapping[consumption[i].type_].pricePerVolume;
            newBill.totalAmount += newDetail.totalAmount;
            newBill.billDetailIds[i] = billDetails.length;
            billDetails.push(newDetail);
        }
        bills.push(newBill);
    }

    function getBillsIdByPeriod(uint256 startDate, uint256 endDate) public view returns (uint256[] memory ids) {
        uint256 count = 0;
        for (uint256 i = 0; i < bills.length; i++) {
            if (startDate <= bills[i].startDate && endDate >= bills[i].endDate) {
                count++;
            }
        }
        uint256[] memory billIds = new uint256[](count);
        uint256 index = 0;
        for (uint256 i = 0; i < bills.length; i++) {
            if (startDate <= bills[i].startDate && endDate >= bills[i].endDate) {
                billIds[index] = bills[i].id;
                index++;
            }
        }
        return billIds;
    }

    function getBillById(uint billId) public view returns (Bill memory bill, BillDetail[] memory details) {
        require(billId < bills.length, "Bill ID does not exist");
        BillDetail[] memory outputDetails = new BillDetail[](bills[billId].billDetailIds.length);
        for (uint256 i = 0; i < bills[billId].billDetailIds.length; i++) {
            outputDetails[i] = billDetails[bills[billId].billDetailIds[i]];
        }
        return (bills[billId], outputDetails);
    }
}
