const express = require('express');
const smartContractManagerService = require('../services/smartContractManagerService');

const router = express.Router();

router.post('/smartContracts', smartContractManagerService.generateSmartContracts);

router.post('/registryGeneration', smartContractManagerService.generateRegistrySC);

router.post('/getRegistryData', smartContractManagerService.getRegistryData);

router.get('/smartContracts', smartContractManagerService.getSmartContractsData);

module.exports = router;