pragma solidity ^0.8.0;

contract SmartContractRegistry {
    struct SmartContract {
        address address_;
        string cron;
        string abi_;
        string label;
        string contractDId;
    }

    mapping(address => SmartContract) public smartContractsByAddress;
    mapping(string => mapping(string => SmartContract)) public smartContractsByLabelAndDId;

    uint256 public testValue;

    function getSmartContractByAddress(address _address) external view returns (SmartContract memory) {
        return smartContractsByAddress[_address];
    }

    function getSmartContractByLabelContractDId(string calldata _label, string calldata _contractDId) external view returns (SmartContract memory) {
        return smartContractsByLabelAndDId[_label][_contractDId];
    }

    function setSmartContract(SmartContract calldata newSmartContract) public {
        smartContractsByAddress[newSmartContract.address_] = newSmartContract;
        smartContractsByLabelAndDId[newSmartContract.label][newSmartContract.contractDId] = newSmartContract;
    }

    function setCron(string calldata _label, string calldata _contractDId, string calldata _cron) public {
        SmartContract storage sc = smartContractsByLabelAndDId[_label][_contractDId];
        sc.cron = _cron;
        smartContractsByAddress[sc.address_].cron = _cron;
    }

    function setAbi(string calldata _label, string calldata _contractDId, string calldata _abi) public {
        SmartContract storage sc = smartContractsByLabelAndDId[_label][_contractDId];
        sc.abi_ = _abi;
        smartContractsByAddress[sc.address_].abi_ = _abi;
    }

    function setLabel(string calldata _label, string calldata _contractDId, string calldata _newLabel) public {
        SmartContract storage sc = smartContractsByLabelAndDId[_label][_contractDId];
        sc.label = _newLabel;

        smartContractsByLabelAndDId[_newLabel][_contractDId] = sc;
        delete smartContractsByLabelAndDId[_label][_contractDId];

        smartContractsByAddress[sc.address_].label = _newLabel;
    }

    function setContractDId(string calldata _label, string calldata _contractDId, string calldata _newContractDId) public {
        SmartContract storage sc = smartContractsByLabelAndDId[_label][_contractDId];
        sc.contractDId = _newContractDId;

        smartContractsByLabelAndDId[_label][_newContractDId] = sc;
        delete smartContractsByLabelAndDId[_label][_contractDId];

        smartContractsByAddress[sc.address_].contractDId = _newContractDId;
    }

    function setTestValue(uint256 _value) public {
        testValue = _value;
    }

    function getTestValue() public view returns (uint256) {
        return testValue;
    }
}
