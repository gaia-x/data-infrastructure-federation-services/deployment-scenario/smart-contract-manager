const deploySmartContract = require('../../utils/deploySmartContract');
const plugins = require('../../plugins');
const { log } = require('winston');
const paymentAbiPath = '../../artifacts/contracts/PaymentChecker.sol/PaymentChecker.json';
const paymentAbi = require('../' + paymentAbiPath).abi
const axios= require('axios');

async function generateSmartContract(data) {
    try {
        const contractDId = data.contractDId
        const body = {
            periodicity: data.payPeriod,
            paymentMethod: data.payMethod,
            paymentSolution: data.paySolution,
            creditorCode: data.creditorCode,
            debitorEmail: data.debitorEmail
        }
        if (!data.billing && data.payAmount && !isNaN(data.payAmount) && data.payAmount.toString().trim()) {
                body.invoices = [{
                        id: 'None',
                        amount: Math.round(parseFloat(data.payAmount) * 100)
                }];
        }
        const contract = await deploySmartContract("", paymentAbiPath);
        await plugins.pluginsExecute('registry', 'saveSmartContractData', {
            address: contract,
            abi: JSON.stringify(paymentAbi),
            label: "payment",
            contractDId: contractDId
        }, {})
        console.log(`Payment smart contract deployed successfully`)
        await axios.post(`${process.env.ORACLE_URL}/payment/cron`, {... body,
            address: contract,
            contractDId: contractDId
        });
        return contract;
    } catch (err) {
        console.log(err);
        throw new Error('Payment SC generation failed')
    }
}

module.exports = { generateSmartContract };




