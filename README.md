
# Smart contract manager
# Sommaire

- [Smart contract manager](#smart-contract-manager)
- [Sommaire](#sommaire)
  - [Introduction ](#introduction-)
  - [General information ](#general-information-)
    - [Usage](#usage)
    - [Why smart contract ?](#why-smart-contract-)
    - [Flexible terms and plugins](#flexible-terms-and-plugins)
    - [Scheme workflow](#scheme-workflow)
  - [Description of the deployment scenario](#description-of-the-deployment-scenario)
    - [Define Contracts with ODRL](#define-contracts-with-odrl)
    - [Smart Contract Manager](#smart-contract-manager-1)
    - [Deploy Smart Contracts](#deploy-smart-contracts)
    - [Front consumption: usage, exemple and integration](#front-consumption-usage-exemple-and-integration)
    - [Oracle Integration](#oracle-integration)
    - [Cron Jobs (if necessary)](#cron-jobs-if-necessary)
    - [Gateway](#gateway)
    - [Results](#results)
  - [Entry point ](#entry-point-)
  - [Architecture ](#architecture-)
  - [Plugins ](#plugins-)
  - [Oracle ](#oracle-)
  - [Routage ](#routage-)
    - [Smart-contract-manager](#smart-contract-manager-2)
    - [Oracle](#oracle)
      - [Payment](#payment)
      - [Billing](#billing)
  - [Step to install the project locally ](#step-to-install-the-project-locally-)
  - [Add a plugin ](#add-a-plugin-)
  - [Technologies and tools used ](#technologies-and-tools-used-)
  - [Authors and acknowledgment ](#authors-and-acknowledgment-)
  - [License ](#license-)

## Introduction <a name="introduction"></a>
The Smart Contract Manager defines the process for users to create and deploy smart contracts efficiently. By utilizing ODRL for policy definitions and integrating real-time data through oracles, this project aims to formalize and enhance the contractualization of digital agreements such as payment or billing.

Thanks to this framework, we have established a comprehensive system enabling providers to create, manage, and deploy smart contracts with ease. This process allows developers to connect to the system using their chosen development environments to build and extend smart contracts. The Smart Contract Manager acts as an intermediary step between defining contract terms in ODRL and deploying these contracts to a blockchain.

## General information <a name="information"></a>

### Usage

The Smart Contract Manager is designed to simplify and enhance the process of creating, managing, and deploying smart contracts, particularly those based on ODRL (Open Digital Rights Language) contracts. This tool is essential for developers and organizations looking to streamline the contractualization of digital agreements. It is particularly useful in environments where dynamic and real-time data integration is crucial, such as in decentralized finance (DeFi) applications, supply chain management, and digital rights management. By leveraging plugins and integrating with oracles, the Smart Contract Manager ensures that contracts are not only defined clearly and accurately but also operate with the most current and relevant data. This makes it a valuable asset for scenarios that require automated, transparent, and enforceable agreements, such as token creation, secure voting systems, and the tracking and management of digital assets. Additionally, its modular and extensible architecture allows for customization and scalability, making it adaptable to various industries and use cases, including gaming, healthcare, and logistics. Overall, the Smart Contract Manager provides a comprehensive solution for deploying robust and reliable smart contracts, enhancing efficiency and trust in digital transactions.

### Why smart contract ?

Smart contracts are essential to the Smart Contract Manager, offering automation, transparency, and security. They streamline processes, ensure immutability, and reduce costs, making them ideal for DeFi, supply chain, and digital rights management. With blockchain security and real-time data from oracles, they provide accurate, transparent billing and payment solutions. The Smart Contract Manager uses these benefits to deliver a robust and efficient platform for managing smart contracts.

### Flexible terms and plugins 

Our Smart Contract Manager features a versatile plugin system, allowing developers to easily extend and customize contract functionalities. This modular approach ensures that new capabilities can be integrated seamlessly. Additionally, ODRL terms can be easily modified or created, providing flexibility in defining and enforcing contract rules. This combination of plugins and customizable ODRL terms ensures a dynamic and adaptable platform for managing smart contracts.

### Scheme workflow

Below is an image describing the different services in the project:

![Texte alternatif](./image/architecture1.jpg)


## Description of the deployment scenario<a name="description"></a>

The deployment process takes place between defining the contract using ODRL and deploying the smart contract via the Smart Contract Manager. The aim is to ensure that smart contracts are created, integrated with oracles, and managed efficiently.

To implement the deployment process, we have set up a series of steps that formalize the creation and execution of smart contracts from ODRL definitions. This process is initiated at the Smart Contract Manager level and can be adapted for various blockchain environments.

When the deployment process begins, all the information concerning the ODRL contract is collected and integrated into the smart contract deployment process.

As far as oracle integration is concerned, it is up to each data provider to create its own oracle plugin. In our case, we have integrated proprietary oracle solutions to ensure real-time data updates.

The deployment process is as follows:

### Define Contracts with ODRL
The process begins with defining the smart contracts using ODRL. These definitions include all the necessary policy rules and conditions for the contract.

### Smart Contract Manager
Once the ODRL definitions are complete, they are fed into the Smart Contract Manager. The Smart Contract Manager interprets these definitions and prepares the smart contracts for deployment.

### Deploy Smart Contracts
The Smart Contract Manager then deploys the smart contracts to the chosen blockchain platform. This step involves compiling the contract code and ensuring it is correctly published on the blockchain.

### Front consumption: usage, exemple and integration

https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/smart-contracts/front-consumption
https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/smart-contracts/api-gateway-consumption
### Oracle Integration
After deployment, the smart contracts are connected to oracles. Oracles provide real-time data necessary for the execution and validation of the smart contracts. This ensures that the contracts operate based on the ost current information.

### Cron Jobs (if necessary)
For contracts requiring periodic updates or checks, cron jobs are set up. These cron jobs automate tasks such as data fetching from oracles or contract condition evaluations, ensuring that the smart contracts remain up-to-date and functional over time.

The tool we are making available can be integrated into various blockchain environments, subject to a few modifications.

### Gateway 

https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/payment-gateway

### Results

## Entry point <a name="entrypoint"></a>

```
{
    "smartContractsToGenerate": [
        "payment", "billing", "consumption"
    ],
        "VC": {
        "type": [
            "VerifiableCredential",
            "ContractCredential"
        ],
            "@context": [
                "https://schema-registry.aster-x.demo23.gxfs.fr/contexts/jws-2020",
                "https://schema-registry.aster-x.demo23.gxfs.fr/contexts/credentials"
            ],
                "id": "did:web:aster-x.demo23.gxfs.fr:d40fccec-ff23-4675-92df-eb04c9cb86ae:data.json",
                    "issuer": "did:web:aster-x.demo23.gxfs.fr",
                        "issuanceDate": "2023-12-20T16:11:56.656Z",
                            "validFrom": "2023-12-20T16:11:56.656Z",
                                "expirationDate": "2024-12-20T16:11:56.656Z",
                                    "proof": {
            "type": "JsonWebSignature2020",
                "created": "2023-12-20T16:11:56.656Z",
                    "proofPurpose": "assertionMethod",
                        "verificationMethod": "did:web:aster-x.demo23.gxfs.fr",
                            "jws": "eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..dLOswgi-NSviRFK4luCjPOil-00gLuF791gYa1TNnfu7wvsL-sokLmmgjtHCTqZTZb5pnrFneY0ScRfmoQbeDh_H7HBmATGIq0N7p30XnPPpU8wCpnKIl9VwXHoAH5Ie-tmTpHkwGJJENSr0ocOFFmshMbVihfsEvwExTrr6Y5wfoEU0GmkReRDSfRT7Zy5i8-4f9vE3UrvJCazCuilekY-83DPGE1IzS4Kpr9ZJP8Y6L7VK9A-_TZKn98qdHgVgZBdvwlO__IRh2NMkIk8Pqf0jEDgh2W-1mBA72xjsRgTjeOv43l1V1KWPsYPxGJsJH0JxEYeiJIFGyKA-wLvDrg"
        },
        "credentialSchema": {
            "id": "https://registry.abc-federation.dev.gaiax.ovh/api/trusted-schemas-registry/v2/schemas/ContractCredential",
                "type": "FullJsonSchemaValidator2021"
        },
        "credentialSubject": {
            "id": "did:web:aster-x.demo23.gxfs.fr",
                "gx-signature:participants": [
                    {
                        "gx-signature:did": "provider.agdatahub@proton.me",
                        "gx-signature:email": "yvan-wilfried.mbe-tene@softeam.fr"
                    },
                    {
                        "gx-signature:did": "jeremy.hirth-daumas@softeam.fr",
                        "gx-signature:email": "jeremy.hirth-daumas@softeam.fr"
                    }
                ],
                    "gx-signature:links": [
                        {
                            "gx-signature:documentId": 64817,
                            "gx-signature:documentName": "terms_and_conditions.pdf",
                            "gx-signature:documentOriginalName": "terms_and_conditions.pdf",
                            "gx-signature:documentLink": "https://www.contralia.fr/eDoc/user-api/document/view?token=UD2c969ea18c1a5dc3018c87fe2d202496-27937148950314067225"
                        },
                        {
                            "gx-signature:documentId": 49932,
                            "gx-signature:documentName": "ODRL.pdf",
                            "gx-signature:documentOriginalName": "ODRL.pdf",
                            "gx-signature:documentLink": "https://www.contralia.fr/eDoc/user-api/document/view?token=UD2c969ea18c1a5dc3018c87fe2d202496-27937148950314067225"
                        }
                    ],
                        "gx:providedBy": "provider.agdatahub@proton.me",
                            "gx:consumedBy": "jeremy.hirth-daumas@softeam.fr",
                                "gx:termOfUsage": "https://example.com/terms-of-usage",
                                    "gx:dataUsage": "https://example.com/data-usage",
                                        "gx:notarizedIn": "https://example.com/notarizedIn",
                                            "gx:dataProduct": {
                "@context": [
                    "https://schema-registry.aster-x.demo23.gxfs.fr/contexts/credentials",
                    "https://schema-registry.aster-x.demo23.gxfs.fr/contexts/jws-2020"
                ],
                    "@type": [
                        "VerifiableCredential"
                    ],
                        "@id": "did:web:agdatahub.provider.demo23.gxfs.fr:participant:eb06c24ac31a16b5f29316b388a742f2487ab626b5d22f23184ea7e8c3a3ed5b/vc/65b155f2810b649b9728a4eaf9239437b1bc34b1961998718080fafbcb3722a9/data.json",
                            "issuer": "did:web:agdatahub.provider.demo23.gxfs.fr",
                                "credentialSubject": {
                    "id": "did:web:agdatahub.provider.demo23.gxfs.fr:participant:eb06c24ac31a16b5f29316b388a742f2487ab626b5d22f23184ea7e8c3a3ed5b/vc/4203ae956f00530e5d68d157949dd3d0a6f07fcff21f3487e730785d53bc1e89/data.json",
                        "type": "gx:ServiceOffering",
                            "gx:providedBy": {
                        "id": "did:web:agdatahub.provider.demo23.gxfs.fr:participant:eb06c24ac31a16b5f29316b388a742f2487ab626b5d22f23184ea7e8c3a3ed5b/data.json"
                    },
                    "gx:termsAndConditions": {
                        "gx:URL": "https://icp-credentialissuer.aster-x.demo23.gxfs.fr/.well-known/TaureauxDeMontePublique",
                            "gx:hash": "c2c8bd85df0023cf7719def1a401b8fe076dacf5d61dc844080322d19ee309d9"
                    },
                    "gx:name": "Taureaux de Monte Publique : déclarations officielles",
                        "aster-conformity:layer": "IAAS",
                            "gx:keyword": [
                                "Data Product"
                            ],
                                "gx:description": "Cette offre propose les données administratives des taureaux déclarés auprès de l’Institut de l’Elevage pour la monte publique. Elle contient les données d’identité, de parenté et de déclarations des taureaux avec leur période de validité, conformément à la réglementation. Le fichier est mis à jour hebdomadairement. Certains taureaux faisant l’objet déclarations complémentaires, un même taureau peut apparaître plusieurs fois dans le fichier.",
                                    "gx:descriptionMarkDown": "",
                                        "gx:webAddress": "",
                                            "gx:dataProtectionRegime": [
                                                "GDPR2016"
                                            ],
                                                "gx:dataAccountExport": [
                                                    {
                                                        "gx:requestType": "email",
                                                        "gx:accessType": "digital",
                                                        "gx:formatType": "mime/png"
                                                    }
                                                ],
                                                    "gx:isAvailableOn": [
                                                        ""
                                                    ],
                                                        "gx:policy": "default: allow",
                                                            "@context": [
                                                                "https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#",
                                                                "https://schema-registry.aster-x.demo23.gxfs.fr/contexts/aster-conformity"
                                                            ],
                                                                "gx-data:dataDomains": "",
                                                                    "gx-data:dataController": "",
                                                                        "gx-data:consent": "",
                                                                            "gx:policies": "",
                                                                                "gx-data:obsoleteDateTime": null,
                                                                                    "gx-data:expirationDateTime": null,
                                                                                        "gx:aggregationOf": [
                                                                                            {
                                                                                                "dct:identifier": "./dataSet/dataset01",
                                                                                                "dct:title": "dataset01",
                                                                                                "gx:description": "",
                                                                                                "gx:copyrightOwnedBy": [
                                                                                                    "France Génétique Elevage"
                                                                                                ],
                                                                                                "gx:personalDataPolicy": "",
                                                                                                "gx:expirationDateTime": null,
                                                                                                "gx:obsoleteDateTime": null,
                                                                                                "dct:distribution": [
                                                                                                    {
                                                                                                        "title": "2023100820231014-TaureauxMP",
                                                                                                        "dct:format": "csv",
                                                                                                        "dcat:byteSize": "5319062",
                                                                                                        "gx:location": "https://platform.api-agro.eu/members/products/data-offerings/a01ed1fbd27245fa85bc9cec23a3e297/view",
                                                                                                        "gx:hash": "",
                                                                                                        "gx:hashAlgorithm": "None",
                                                                                                        "gx:expirationDateTime": null,
                                                                                                        "gx:obsoleteDateTime": null
                                                                                                    }
                                                                                                ]
                                                                                            }
                                                                                        ]
                },
                "issuanceDate": "2023-11-07T12:20:57.161303+00:00",
                    "proof": {
                    "type": "JsonWebSignature2020",
                        "proofPurpose": "assertionMethod",
                            "verificationMethod": "did:web:agdatahub.provider.demo23.gxfs.fr",
                                "created": "2023-11-07T12:20:57.161303+00:00",
                                    "jws": "eyJhbGciOiAiUFMyNTYiLCAiYjY0IjogZmFsc2UsICJjcml0IjogWyJiNjQiXX0..WCn13wS8pZMsYJcC2tXdZXn385PauLGRvHVLBXSl5Va_C7QP-6NNugswDMOoX36x-bkjkrn4zc4Itse4zu00EEZH9S5nfHbThfWYvBkKiLhk3A8Y6AWTievWeL-aC7FSqDTqn8yT17v8DCxm_HJyUyRaGCSHjKhgAxDDwRkXeiNqZYn1M1nR-5Uyakivve4U1Ffj6kNWjFEJ-k7DqoywZGQnGvAf-B1aoTSAsErJ9dNlKdPe4bFAE9yc0trjhBKToINdbwRE5TJ-UldAbjiHZfFhMJ8dFqF0zyym-L8X6exaRLNV6HaU0K_wTX6FH1HYPcb2QSwVmnYDHV_BSoxg0Q"
                }
            },
            "odrl:hasPolicy": {
                "@context": "https://www.w3.org/ns/odrl.jsonld",
                    "@type": "Set",
                        "uid": "https://agdatahub.provider.demo23.gxfs.fr/policy:1",
                            "nomService": "agdatahub",
                                "permission": [
                                    {
                                        "target": "https://minio.demo23.gxfs.fr/dataproduct/dataproduct01",
                                        "assignee": "Data Subscriber",
                                        "action": "use",
                                        "duty": [
                                            {
                                                "assigner": "Data Provider Inc.",
                                                "assignee": "Data Subscriber",
                                                "action": [
                                                    {
                                                        "rdf:value": {
                                                            "@id": "odrl:compensate"
                                                        },
                                                        "refinement": [
                                                            {
                                                                "leftOperand": "payAmount",
                                                                "operator": "eq",
                                                                "rightOperand": {
                                                                    "@value": "135.00",
                                                                    "@type": "xsd:decimal"
                                                                },
                                                                "unit": "http://dbpedia.org/resource/Euro"
                                                            },
                                                            {
                                                                "leftOperand": "timeInterval",
                                                                "operator": "eq",
                                                                "rightOperand": "PT60S"
                                                            },
                                                            {
                                                                "leftOperand": "payMethod",
                                                                "operator": "eq",
                                                                "rightOperand": "creditCard"
                                                            },
                                                            {
                                                                "leftOperand": "paySolution",
                                                                "operator": "eq",
                                                                "rightOperand": "paynum"
                                                            },
                                                            {
                                                                "leftOperand": "creditorCode",
                                                                "operator": "eq",
                                                                "rightOperand": "gaia-x"
                                                            },
                                                            {
                                                                "leftOperand": "debitorEmail",
                                                                "operator": "eq",
                                                                "rightOperand": "moez.somai@softeam.fr"
                                                            }
                                                        ]
                                                    },
                                                    {
                                                        "rdf:value": {
                                                            "@id": "odrl:billing"
                                                        },
                                                        "periodicity": {
                                                            "leftOperand": "timeInterval",
                                                            "operator": "eq",
                                                            "rightOperand": "PT20S"
                                                        },
                                                        "refinement": [
                                                            [
                                                                {
                                                                    "leftOperand": "type",
                                                                    "operator": "eq",
                                                                    "rightOperand": "CSV"
                                                                },
                                                                {
                                                                    "leftOperand": "volume",
                                                                    "operator": "eq",
                                                                    "rightOperand": {
                                                                        "@value": "5"
                                                                    },
                                                                    "unit": "https://dbpedia.org/page/Gigabyte"
                                                                },
                                                                {
                                                                    "leftOperand": "pricePerVolume",
                                                                    "operator": "eq",
                                                                    "rightOperand": {
                                                                        "@value": "10.00",
                                                                        "@type": "xsd:decimal"
                                                                    },
                                                                    "unit": "http://dbpedia.org/resource/Euro"
                                                                }
                                                            ],
                                                            [
                                                                {
                                                                    "leftOperand": "type",
                                                                    "operator": "eq",
                                                                    "rightOperand": "XML"
                                                                },
                                                                {
                                                                    "leftOperand": "volume",
                                                                    "operator": "eq",
                                                                    "rightOperand": {
                                                                        "@value": "3"
                                                                    },
                                                                    "unit": "https://dbpedia.org/page/Gigabyte"
                                                                },
                                                                {
                                                                    "leftOperand": "pricePerVolume",
                                                                    "operator": "eq",
                                                                    "rightOperand": {
                                                                        "@value": "15.00",
                                                                        "@type": "xsd:decimal"
                                                                    },
                                                                    "unit": "http://dbpedia.org/resource/Euro"
                                                                }
                                                            ]
                                                        ]
                                                    }
                                                ]
                                            },
                                            {
                                                "action": "nextPolicy",
                                                "target": "https://agdatahub.provider.demo23.gxfs.fr/policy:1010"
                                            }
                                        ],
                                        "constraint": [
                                            {
                                                "leftOperand": "dataVolumePerDay",
                                                "operator": "lteq",
                                                "rightOperand": 5,
                                                "unit": "https://dbpedia.org/page/Gigabyte"
                                            },
                                            {
                                                "leftOperand": "dataVolume",
                                                "operator": "lteq",
                                                "rightOperand": 50,
                                                "unit": "https://dbpedia.org/page/Gigabyte"
                                            },
                                            {
                                                "leftOperand": "odrl:spatial",
                                                "operator": "isAnyOf",
                                                "rightOperand": {
                                                    "@list": [
                                                        "Europe",
                                                        "États-Unis"
                                                    ]
                                                }
                                            },
                                            {
                                                "leftOperand": "odrl:industry",
                                                "operator": "isAnyOf",
                                                "rightOperand": {
                                                    "@list": [
                                                        "Technologie"
                                                    ]
                                                }
                                            },
                                            {
                                                "leftOperand": "odrl:product",
                                                "operator": "isAnyOf",
                                                "rightOperand": {
                                                    "@list": [
                                                        "Données Financières"
                                                    ]
                                                }
                                            },
                                            {
                                                "leftOperand": "dateTime",
                                                "operator": "gteq",
                                                "rightOperand": {
                                                    "@value": "2025-09-15",
                                                    "@type": "xsd:date"
                                                }
                                            },
                                            {
                                                "leftOperand": "dateTime",
                                                "operator": "lt",
                                                "rightOperand": {
                                                    "@value": "2025-09-15",
                                                    "@type": "xsd:date"
                                                }
                                            },
                                            {
                                                "leftOperand": "fileFormat",
                                                "operator": "in",
                                                "rightOperand": [
                                                    "application/pdf",
                                                    "image/jpeg"
                                                ]
                                            }
                                        ]
                                    }
                                ],
                                    "obligation": [
                                        {
                                            "target": "https://minio.demo23.gxfs.fr/dataset/dataset01",
                                            "assigner": "Data Subscriber",
                                            "assignee": "Data Provider Inc.",
                                            "action": "distribute"
                                        }
                                    ]
            },
            "type": "ContractCredential"
        },
        "validUntil": "2024-12-20T16:11:56.656Z",
            "credentialStatus": {
            "type": "StatusList2021Entry",
                "id": "https://revocation-registry.aster-x.demo23.gxfs.fr/api/v1/revocations/credentials/default-suspension#841",
                    "statusPurpose": "suspension",
                        "statusListIndex": "841",
                            "statusListCredential": "https://revocation-registry.aster-x.demo23.gxfs.fr/api/v1/revocations/credentials/default-suspension"
        }
    }
}
```
## Architecture <a name="architecture"></a>

## Plugins <a name="plugins"></a>

## Oracle <a name="oracle"></a>

## Routage <a name="routage"></a>


### Smart-contract-manager

{URL} : The url of the Smart-contract-manager

| HTTP Verb | url                                           | input               | output | Description                                                  |
|-----------|-----------------------------------------------|---------------------|-|--------------------------------------------------------------|
| HTTP POST | https://{URL}/smartContracts | application/json    | application/json  | Deploy smart contracts from ODRL |
| HTTP POST | https://{URL}/registryGeneration | n/a    | application/json | Allows you to connect a new dataspace                        |
| HTTP POST | https://{URL}/getRegistryData | application/json | application/json  | X                          |
| HTTP GET  | https://{URL}/smartContracts | n/a                 | application/json  | Get datas from smart contract in registry                          |

### Oracle

{URL} : The url of the Oracle

#### Payment

| HTTP Verb | url                                           | input               | output | Description                                                  |
|-----------|-----------------------------------------------|---------------------|-|--------------------------------------------------------------|
| HTTP POST | https://{URL}/payment | application/json    | application/json  | Generate payment manually   |
| HTTP GET | https://{URL}/payment | n/a    | application/json | Get information about payments                        |
| HTTP POST | https://{URL}/payment/cron | application/json | application/json  | Generate cron payment manually               |

#### Billing

| HTTP Verb | url                                           | input               | output | Description                                                  |
|-----------|-----------------------------------------------|---------------------|-|--------------------------------------------------------------|
| HTTP POST | https://{URL}/bill/ | application/json    | application/json  | Generate bill manually   |
| HTTP GET | https://{URL}/bill | n/a    | application/json | Get information about bills                        |
| HTTP POST | https://{URL}/bill/cron | application/json | application/json  | Generate cron payment manually               |
| HTTP POST | https://{URL}/bill/period | application/json    | application/json  | Get bills between two periods   |
| HTTP POST | https://{URL}/bill/currency | n/a    | application/json | Set currency for an existing smart contract                        |
| HTTP POST | https://{URL}/bill/pricing | application/json | application/json  | Set pricing for an existing smart contract          |


## Step to install the project locally <a name="step-to-install-the-project-locally"></a>

```
git clone git@gitlab.com:gaia-x/data-infrastructure-federation-services/deployment-scenario/smart-contract-manager.git
cd ./smart-contract-manager/
npm i
npx hardhat compile
npx hardhat node
```
.env such as 
```
EVM_NODE_URL=HARDHATURL
PRIVATE_KEY=HARDHATPRIVATEKEY
ORACLE_URL=ORACLEURL
```
In another instance 
```
cd ./smart-contract-manager/
npx run start
```

## Add a plugin <a name="add-plugin"></a>


## Technologies and tools used <a name="technology-and-tool"></a>

- Solidity 
- Ethereum
- hardhat
- Ethers.js
- paynum

## Authors and acknowledgment <a name="authors-and-acknowledgment"></a>
GXFS-FR

## License <a name="licence"></a>
The Negotiation Tool example is delivered under the terms of the Apache License Version 2.0.
