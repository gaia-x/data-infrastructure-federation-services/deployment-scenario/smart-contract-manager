pragma solidity ^0.8.0;

contract ConsumptionChecker {
    struct Data {
        string _type;
        uint date;
        uint volume;
    }

    Data[] public dataList;

    event DataWritten(Data[] datas);

    function writeData(Data[] calldata newDatas) public {
        for (uint i = 0; i < newDatas.length; i++) {
            dataList.push(newDatas[i]);
        }
    }

    function getDataByPeriod(uint startDate, uint endDate) public view returns (Data[] memory datas) {
        uint count = 0;
        for (uint i = 0; i < dataList.length; i++) {
            if (dataList[i].date >= startDate && dataList[i].date <= endDate) {
                count++;
            }
        }

        Data[] memory result = new Data[](count);
        uint index = 0;
        for (uint i = 0; i < dataList.length; i++) {
            if (dataList[i].date >= startDate && dataList[i].date <= endDate) {
                result[index] = dataList[i];
                index++;
            }
        }

        return result;
    }

    function tmp() public view returns (Data[] memory datas) { 
        return dataList;
    }
}