const jsonpath = require('jsonpath');

const plugins = require('../plugins.js');
const configSmartContracts = [
    {
        contractName: 'location',
        values: [
            { key: 'contractDId', path: '$.VC.id' },
            { key: 'spatialTerm', path: '$..[?(@.duty)].constraint[?(@.leftOperand === "odrl:spatial")]' },
        ]
    },
    {
        contractName: 'payment',
        values: [
            { key: 'contractDId', path: '$.VC.id' },
            { key: 'payPeriod', path: '$..[?(@.duty)].duty[?(@.assignee === "Data Subscriber")].action[?(@.refinement)].refinement[?(@.leftOperand === "timeInterval")].rightOperand' },
            { key: 'payAmount', path: '$..[?(@.duty)].duty[?(@.assignee === "Data Subscriber")].action[?(@.refinement)].refinement[?(@.leftOperand === "payAmount")].rightOperand["@value"]' },
            { key: 'payMethod', path: '$..[?(@.duty)].duty[?(@.assignee === "Data Subscriber")].action[?(@.refinement)].refinement[?(@.leftOperand === "payMethod")].rightOperand' },
            { key: 'paySolution', path: '$..[?(@.duty)].duty[?(@.assignee === "Data Subscriber")].action[?(@.refinement)].refinement[?(@.leftOperand === "paySolution")].rightOperand' },
            { key: 'creditorCode', path: '$..[?(@.duty)].duty[?(@.assignee === "Data Subscriber")].action[?(@.refinement)].refinement[?(@.leftOperand === "creditorCode")].rightOperand' },
            { key: 'debitorEmail', path: '$..[?(@.duty)].duty[?(@.assignee === "Data Subscriber")].action[?(@.refinement)].refinement[?(@.leftOperand === "debitorEmail")].rightOperand' }],
    },
    {
        contractName: 'billing',
        arrayPath: '$..[?(@.duty)].duty[?(@.assignee === "Data Subscriber")].action[?(@["rdf:value"]["@id"] === "odrl:billing")].refinement',
        values: [
            // { key: 'contractDId', path: '$.VC.id' },
            { key: 'type', path: '$..[?(@.leftOperand === "type")].rightOperand' },
            { key: 'volume', path: '$..[?(@.leftOperand === "volume")].rightOperand["@value"]' },
            { key: 'volumeType', path: '$..[?(@.leftOperand === "volume")].unit' },
            { key: 'pricePerVolume', path: '$..[?(@.leftOperand === "pricePerVolume")].rightOperand["@value"]' }
        ]
    },
    {
        contractName: 'consumption',
        values: [
            { key: 'contractDId', path: '$.VC.id' }
        ]
    },
    {
        contractName: 'limit',
        values: [
            { key: 'contractDId', path: '$.VC.id' },
            { key: 'overallLimitTerm', path: '$..[?(@.duty)].constraint[?(@.leftOperand === "dataVolume")]' },
            { key: 'dailyLimitTerm', path: '$..[?(@.duty)].constraint[?(@.leftOperand === "dataVolumePerDay")]' }
        ]
    },
];

// Handler for generating SCs
async function generateSmartContracts(req, res) {
    const odrl = req.body.odrl;

    for (const { contractName, values, arrayPath } of configSmartContracts) {
        if (odrl.smartContractsToGenerate.includes(contractName)) {
            const params = {};

            if (arrayPath) {
                params.billing = { pricing: [] };
                const items = jsonpath.query(odrl, arrayPath)[0];

                for (const item of items) {
                    const pricing = values.map(({ key, path }) => {
                        const value = jsonpath.query(item, path)[0];
                        return key === "pricePerVolume" ? Math.round(parseFloat(value) * 100) : value;
                    });
                    params.billing.pricing.push(pricing);
                }
            } else {
                values.forEach(({ key, path }) => {
                    params[key] = jsonpath.query(odrl, path)[0];
                });
            }

            try {
                await plugins.pluginsExecute(contractName, 'generateSmartContract', params, {});
                console.log(`Smart Contract for ${contractName} generated successfully`);
            } catch (err) {
                console.error(`Smart Contract generation for ${contractName} failed`, err);
                throw new Error(`Smart Contract generation for ${contractName} failed`);
            }
        }
    }
}


function generateRegistrySC(req, res) {
    plugins.pluginsExecute('registry', 'generateSmartContract', {}, {})
        .then((result, err) => {
            if (err) {
                throw new Error('SC generation failed')
            }
            res.status(200).json({ message: 'Smart Contracts generated successfully', contracts: result });
        })
}

function getRegistryData(req, res) {
    plugins.pluginsExecute('registry', 'retrieveRegistryData', {}, {})
        .then((result, err) => {
            if (err) {
                throw new Error('Retrieving registry SC info failed')
            }
            res.status(200).json({ message: 'Smart Contracts registry infos retrieved successfully', infos: result });
        })
}

function getSmartContractsData(req, res) {
    plugins.pluginsExecute('registry', 'retrieveSmartContractData', {address: req.query.address, label: req.query.label, contractDId: req.query.contractDId}, {}) //req.query.address
        .then((result, err) => {
            if (err) {
                throw new Error('Retrieving SC info failed')
            }
            res.status(200).json({
                message: 'Smart Contracts infos retrieved successfully', infos: {
                    address: result[0],
                    cron: result[1],
                    abi: result[2],
                    label: result[3],
                    contractDId: result[4]

                }
            });
        })
}

module.exports = {
    generateSmartContracts,
    generateRegistrySC,
    getRegistryData,
    getSmartContractsData
};