pragma solidity ^0.8.24;

contract DataLoggerConsumption {
    struct Data {
        string userId;
        string date;
        string ip;
        string fileName;
        uint fileSize;
        string fileType;
    }

    Data[] public dataList;

    event DataWritten(string userId, string date, string ip, string fileName, uint fileSize, string fileType);

    function writeData(string memory userId, string memory date, string memory ip, string memory fileName, uint fileSize, string memory fileType) public {
        dataList.push(Data(userId, date, ip, fileName, fileSize, fileType));
        emit DataWritten(userId, date, ip, fileName, fileSize, fileType);
    }

    function getDataByUserId(string memory userId) public view returns (string[] memory dates, string[] memory ips, string[] memory fileNames, uint[] memory fileSizes, string[] memory fileTypes) {
        string[] memory memoryDates = new string[](dataList.length);
        string[] memory memoryIps = new string[](dataList.length);
        string[] memory memoryFileNames = new string[](dataList.length);
        uint[] memory memoryFileSizes = new uint[](dataList.length);
        string[] memory memoryFileTypes = new string[](dataList.length);
        
        uint count = 0;
        for (uint i = 0; i < dataList.length; i++) {
            if (keccak256(bytes(dataList[i].userId)) == keccak256(bytes(userId))) {
                memoryDates[count] = dataList[i].date;
                memoryIps[count] = dataList[i].ip;
                memoryFileNames[count] = dataList[i].fileName;
                memoryFileSizes[count] = dataList[i].fileSize;
                memoryFileTypes[count] = dataList[i].fileType;
                count++;
            }
        }
        return (memoryDates, memoryIps, memoryFileNames, memoryFileSizes, memoryFileTypes);
    }
}